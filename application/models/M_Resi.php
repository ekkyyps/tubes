<?php
 // write your name and student id here
class Resi_model extends CI_model
{
	public function getAllInvoice()
	{
		//use query builder to get data table "listongkir"
		$query = $this->db->get('resi');
		return $query->result_array();
  }
    
  public function cariInvoice()
	{
		$keyword = $this->input->post('keyword', true);
		//use query builder class to search data ongkir based on keyword "Kota Tujuan"
		$this->db->like('invoice', $keyword);
		//return data mahasiswa that has been searched
		$query = $this->db->get('resi');
		return $query->result_array();
	}
}
