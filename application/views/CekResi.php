<div class="container">
    <div class="row mt-3">
        <div class="col md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Input nomor invoice" name="keyword">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#edit1">Cari</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Cek Mahasiswa -->
<div class="modal fade" id="edit1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <center><h2>Cek Resi</h2></center>
      </div>
      <div class="modal-body">

      <!-- isi form ini -->
      <div class="container">    
            <div class="row mt-5">
                <div class="col">
                    <h3 class="text-center">List Tarif Ongkir</h3>
                    <?php if (empty($resi)) : ?>
                    <!-- <div class="alert alert-danger" role="alert">
                        Data tidak ditemukan
                    </div> -->
                    <table class="table mt-5">
                        <thead>
                            <tr>
                                <th class="text-center" scope="col">INVOICE</th>
                                <th class="text-center" scope="col">PENERIMA</th>
                                <th class="text-center" scope="col">PENGIRIM</th>
                                <th class="text-center" scope="col">ALAMAT</th>
                                <th class="text-center" scope="col">KOTA</th>
                                <th class="text-center" scope="col">DESKRIPSI</th>
                                <th class="text-center" scope="col">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><?php foreach ($resi as $r) : ?>
                                <td class="text-center"><?= $r['invoice']; ?></td>
                                <td class="text-center"><?= $r['penerima']; ?></td>
                                <td class="text-center"><?= $r['pengirim']; ?></td>
                                <td class="text-center"><?= $r['alamat']; ?></td>
                                <td class="text-center"><?= $r['kota']; ?></td>
                                <td class="text-center"><?= $r['deskripsi']; ?></td>
                                <td class="text-center"><?= $r['status']; ?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?php endif; ?>

                    
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>